"""
    entrypoint.py:: spawn routes, handle requests

"""

import falcon


class QuoteResource:

    def on_get(self, request, response):
        quote = {
            'quote': (
                "Hello, world"
            ),
        }

        response.media = quote


api = falcon.API()
api.add_route('/get', QuoteResource())

