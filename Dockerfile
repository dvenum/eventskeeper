#
#
#

FROM python:3.8-buster
WORKDIR /code

# not for deploy
ENV PYTHONUNBUFFERED 1

COPY requirements.txt requirements.txt

#RUN apt update

RUN pip3 install -r requirements.txt
