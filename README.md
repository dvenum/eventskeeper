Eventskeeper
=================

Store http links, save timestamp, browse stored info.

Funbox test assignment:
https://funbox.ru/q/python.pdf
Maxim Bulatov  dvenum.at@gmail.com  May 2020


Installation
=================

		git clone git@gitlab.com:dvenum/eventskeeper.git
		cd eventskeeper
		docker-compose build

Tests
=================
		docker-compose up test

Run
=================

start web on 0.0.0.0:8000

		docker-compose up -d eventskeeper


Development
=================


Default credentials
=================

	see .env file


API samples
=================


Remarks
================

* Redis server dump in AOF mode, looks good on this case, but experiment is needed, if real usage.
* Falcon is well for microservices, not suitable for html pages. Tornado may be alternative here, depends from real case.
* vm.overcommit_memory should be set to 1 or fork may report about allocation error on save.

