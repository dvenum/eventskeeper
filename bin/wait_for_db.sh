#!/usr/bin/env bash

host=${HOST:-redis}
port=${REDIS_PORT:-6379}
wait_period=${WAIT_PERIOD:-1}

until cat < /dev/null > /dev/tcp/$host/$port; do
  echo "Redis is unavailable. waiting for $wait_period seconds"
  sleep $wait_period
done

